﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnClickSprite : MonoBehaviour
{
    void OnMouseDown()
    {
        Debug.Log("Sprite Clicked ( " + this.gameObject.GetComponent<SpriteRenderer>().color + this.gameObject.GetComponent<CorrectBoxController>().isCorrect + ")");
        if (this.gameObject.GetComponent<CorrectBoxController>().isCorrect)
        {
            SoundManager.PlaySound("correct");
            GameObject.Find("GameManager").GetComponent<GameManager>().auxScore = (SceneManager.GetActiveScene().buildIndex == 2) ? 100 : 5;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            GameObject.Find("GameManager").GetComponent<GameManager>().auxLifes = -1;
        }
        else
        {
            SoundManager.PlaySound("wrong");
            GameObject.Find("GameManager").GetComponent<GameManager>().auxLifes = 1;
        }
    }
}
