﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUITextController : MonoBehaviour
{
    public Text score;
    public Text click;
    public Text result;
    public Text finalScore;
}
