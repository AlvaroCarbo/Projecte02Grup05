﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public Button level1;
    public Button level2;
    public Button level3;
    public Button level4;
    public Button back;

    void Start()
    {
        GameManager.score = 0;
        level1.onClick.AddListener(StartLevel1);
        level2.onClick.AddListener(StartLevel2);
        level3.onClick.AddListener(StartLevel3);
        level4.onClick.AddListener(StartLevel4);
        back.onClick.AddListener(BackToMenu);
    }
    public void StartLevel1()
    {
        SceneManager.LoadScene("Level1Scene");
    }
    public void StartLevel2()
    {
        SceneManager.LoadScene("Level2Scene");
    }
    public void StartLevel3()
    {
        SceneManager.LoadScene("Level3Scene");
    }
    public void StartLevel4()
    {
        SceneManager.LoadScene("Level4Scene");
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene("StartScene");
    }
}
