﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip buttonClick, rightClick, wrongClick;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        buttonClick = Resources.Load<AudioClip>("click");
        rightClick = Resources.Load<AudioClip>("correct");
        wrongClick = Resources.Load<AudioClip>("wrong");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "click":
                audioSrc.PlayOneShot(buttonClick);
                break;
            case "correct":
                audioSrc.PlayOneShot(rightClick);
                break;
            case "wrong":
                audioSrc.PlayOneShot(wrongClick);
                break;
        }
    }
}
