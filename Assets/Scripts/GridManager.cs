﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GridManager : MonoBehaviour
{
    [SerializeField]
    private int rows = 18;
    [SerializeField]
    private int cols = 18;
    [SerializeField]
    private float tileSize = 9.5f;
    [SerializeField]
    private int [,] correctBox = new int [3,3];
    // Start is called before the first frame update

    void Start()
    {
        Application.targetFrameRate = 10;
        QualitySettings.vSyncCount = 0;

        GenerateGrid();
        GenerateCorrectBox();
    }

    void GenerateCorrectBox()
    {
        correctBox[0, 0] = Random.Range(0, (rows - 1) * (cols - 1));

        for (int i = 1; i <= rows; i++)
            correctBox[0, 0] = (correctBox[0, 0] % ((rows * i) - 1) == 0 && correctBox[0, 0] != 0) ? correctBox[0, 0] -= 2 : ((correctBox[0, 0] % ((rows * i) - 2) == 0 && correctBox[0, 0] != 0) ? correctBox[0, 0] -= 1 : correctBox[0, 0]);
        
        for (int i = 0; i < correctBox.GetLength(0); i++)
            for (int j = 0; j < correctBox.GetLength(1); j++)
                correctBox[i, j] = correctBox[0, 0] + j + (i * rows);

        for (int i = 0; i < rows * cols; i++)
        {
            GameObject tile = GameObject.Find("GridHolder").transform.GetChild(i).gameObject;

            for (int j = 0; j < correctBox.GetLength(0); j++)
                for (int k = 0; k < correctBox.GetLength(1); k++)
                    if (correctBox[j, k] == i)
                        tile.GetComponent<CorrectBoxController>().isCorrect = true;
        }
    }

    void GenerateGrid(){
        GameObject referenceTile = (GameObject)Instantiate(Resources.Load("Square"));

        for (int row = 0; row < rows; row++){
            for (int col = 0; col < cols; col++){
                GameObject tile =(GameObject)Instantiate(referenceTile, transform);

                float posX = col * tileSize;
                float posY = row * tileSize;
                //Debug.Log(col.ToString() + " " + row.ToString());
                tile.transform.position = new Vector2(posX, posY);
            }
        }

        Destroy(referenceTile);

        float gridW = cols * tileSize;
        float gridH = rows * tileSize;
        transform.position = new Vector2(-gridW / 2 + tileSize / 2, gridH / 2 - tileSize / 2);
    }

    void ChangeColorLevel1()
    {
        for (int i = 0; i < rows * cols; i++)
        {
            GameObject tile = GameObject.Find("GridHolder").transform.GetChild(i).gameObject;
            float color = Random.Range(0.1f, 0.5f);
            tile.GetComponent<SpriteRenderer>().color = new Color(color, color, color);
            for (int j = 0; j < correctBox.GetLength(0); j++)
                for (int k = 0; k < correctBox.GetLength(1); k++)
                    if (correctBox[j,k] == i)
                    {
                        color = Random.Range(0.7f, 0.9f);
                        tile.GetComponent<SpriteRenderer>().color = new Color(color, color, color);
                    }
        }
    }

    void ChangeColorLevel2()
    {
        for (int i = 0; i < rows * cols; i++)
        {
            GameObject tile = GameObject.Find("GridHolder").transform.GetChild(i).gameObject;
            tile.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0.3f, 0.7f), 0, Random.Range(0.2f, 0.3f));
            for (int j = 0; j < correctBox.GetLength(0); j++)
                for (int k = 0; k < correctBox.GetLength(1); k++)
                    if (correctBox[j, k] == i)
                    {
                        float diff = (float) GameManager.score / 400f;
                        float color = (Random.Range(0.75f - diff, 1f - diff));
                        Debug.Log(diff + " " + color);
                        tile.GetComponent<SpriteRenderer>().color = new Color(color, 0, Random.Range(0.2f, 0.3f));
                    }
        }
    }

    void ChangeColorLevel3()
    {
        for (int i = 0; i < rows * cols; i++)
        {
            GameObject tile = GameObject.Find("GridHolder").transform.GetChild(i).gameObject;
            tile.GetComponent<SpriteRenderer>().color = new Color(0, Random.Range(0.5f, 1f), 0);
            for (int j = 0; j < correctBox.GetLength(0); j++)
                for (int k = 0; k < correctBox.GetLength(1); k++)
                    if (correctBox[j, k] == i)
                    {
                        float diff = (float)GameManager.score / 150f;
                        float colorG = (Random.Range(0f + diff, 0.4f + diff));
                        float colorR = (Random.Range(0.8f - diff, 1f - diff));
                        Debug.Log(diff + " " + colorR);
                        tile.GetComponent<SpriteRenderer>().color = new Color(colorR, Random.Range(0.5f, 1f), 0);
                    }
        }
    }

    void ChangeColorLevel4()
    {
        for (int i = 0; i < rows * cols; i++)
        {
            GameObject tile = GameObject.Find("GridHolder").transform.GetChild(i).gameObject;
            tile.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0f, 0.4f), Random.Range(0.4f, 0.7f), 1);
            for (int j = 0; j < correctBox.GetLength(0); j++)
                for (int k = 0; k < correctBox.GetLength(1); k++)
                    if (correctBox[j, k] == i)
                    {
                        float diff = (float)GameManager.score / 200f;
                        float colorR = (Random.Range(0.7f - diff, 0.9f - diff));
                        Debug.Log(diff + " " + colorR);
                        tile.GetComponent<SpriteRenderer>().color = new Color(colorR, Random.Range(0.4f, 0.7f), 1);
                    }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            ChangeColorLevel1();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            ChangeColorLevel2();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 4)        {
            ChangeColorLevel3();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 5)
        {
            ChangeColorLevel4();
        }

    }
}
