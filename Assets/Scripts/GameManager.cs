﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static int score = 0;
    static int lifes = 1;
    public int auxScore;
    public int auxLifes;
    public Button restartButton;
    public Button exitButton;


    void Start()
    {
        restartButton.onClick.AddListener(LoadGameScene);
        exitButton.onClick.AddListener(LoadMenuScene);
    }

    // Update is called once per frame
    void Update()
    {
        lifes -= auxLifes;
        if (auxLifes == 1)
            auxLifes = 0;

        GameObject.Find("Canvas").GetComponent<GUITextController>().score.text = "Percentage: " + score.ToString() + "%";
        
        if (score < 100)
        {
            int temp = score;
            score += auxScore;
            if (temp != score)
                lifes = 1;
        } else if (score == 100)
        {
            LoadResultScene();
        }

        if (lifes == 0)
            GameObject.Find("Canvas").GetComponent<GUITextController>().click.text = "Try again!";
        else if (lifes < 0)
        {
            LoadResultScene();
        } else
            GameObject.Find("Canvas").GetComponent<GUITextController>().click.text = "Click on the small square!";
    }

    void LoadResultScene()
    {
        SceneManager.LoadScene("ResultScene");
        lifes = 1;
    }

    void LoadMenuScene()
    {
        
        SceneManager.LoadScene("LevelSelector");
        score = 0;
        lifes = 1;
    }
    void LoadGameScene()
    {
        SoundManager.PlaySound("click");
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex == 6) ? 1 : SceneManager.GetActiveScene().buildIndex);
        score = 0;
        lifes = 1;
    }
}
