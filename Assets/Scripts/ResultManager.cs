﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultManager : MonoBehaviour
{
    public Button restartButton;
    public Button exitButton;
    // Start is called before the first frame update
    void Start()
    {
        restartButton.onClick.AddListener(LoadGameScene);
        exitButton.onClick.AddListener(LoadMenuScene);
    }

    // Update is called once per frame
    void Update()
    {
        showResult();
    }

    void showResult()
    {
        Debug.Log(GameManager.score);
        if (GameManager.score == 100)
        {
            GameObject.Find("Canvas").GetComponent<GUITextController>().finalScore.text = GameManager.score.ToString() + "%";
            GameObject.Find("Canvas").GetComponent<GUITextController>().result.text = "You win! 100/100";
        } else
        {
            GameObject.Find("Canvas").GetComponent<GUITextController>().finalScore.text = GameManager.score.ToString() + "%";
            GameObject.Find("Canvas").GetComponent<GUITextController>().result.text = "At least you tried...";
        }
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene("LevelSelector");
    }
    public void LoadMenuScene()
    {
        SceneManager.LoadScene("StartScene");
    }
}
