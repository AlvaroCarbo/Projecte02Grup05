﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public Button startButton;

    void Start()
    {
        startButton.onClick.AddListener(StartGame);
    }
    public void StartGame()
    {
        SoundManager.PlaySound("click");
        SceneManager.LoadScene("LevelSelector");
    }
}
